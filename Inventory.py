'''
This program add and remove quantity in a inventory data base in csv format

made by Karl Delorme 2018
Note: v2 will automatically found description and quantity no need to specify BOM or order
'''


import csv

#Create add function
def add(oldcsv,newcsv, addcsv, quantity_old, quantity_new,multi):

#open old inventory
    oldcsv_file = open(oldcsv, "r")
    oldinvcsv = csv.reader(oldcsv_file, delimiter=',', quotechar='"')

#transfert old inventory file in a manipulable list
    oldinv = []
    for row in oldinvcsv:
        oldinv.append(row)

#open order
    addcsv_file = open(addcsv, "r")
    new = csv.reader(addcsv_file, delimiter=',', quotechar='"')

#compare description in old inventory and order and add quantity to inventory
    for row in oldinv:
        addcsv_file.seek(0)
        for row2 in new:
            if row[0] == row2[4]:
                if row[0] != "Description" and row2[4] != "Description":
                    row[quantity_old] = str(int(row[quantity_old]) + int(multi)*int(row2[quantity_new]))

#if item dont existe in old inventory we add it at the end of inventory
    descriptions = []
    for row in oldinv:
        descriptions.append(row[0])

    addcsv_file.seek(0)
    for row2 in new:
        if row2[4] not in descriptions:
            if row2[4] != "":
                quantity = str(int(multi)*int(row2[quantity_new]))
                oldinv.append([row2[4],"","","","","", quantity])

#write inventory in new inventory file
    with open(newcsv, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                            quotechar='"')
        writer.writerows(oldinv)

#close all files
    oldcsv_file.close()
    addcsv_file.close()

#Create remove function
def sub(oldcsv,newcsv, subcsv, quantity_old, quantity_remove,multi):

#open old inventory
    oldcsv_file = open(oldcsv, "r")
    oldinvcsv = csv.reader(oldcsv_file, delimiter=',', quotechar='"')

#transfert old inventory file in a manipulable list
    oldinv = []
    for row in oldinvcsv:
        oldinv.append(row)

#open BOM
    subcsv_file = open(subcsv, "r")
    consume = csv.reader(subcsv_file, delimiter=',', quotechar='"')

#compare description in old inventory and BOM and add quantity to inventory
    for row in oldinv:
        subcsv_file.seek(0)
        for row2 in consume:
            if row[0] == row2[0]:
                if row[0] != "Description" and row2[0] != "Description":
                    row[quantity_old] = str(int(row[quantity_old]) - int(multi)*int(row2[quantity_remove]))

#if item dont existe in old inventory we add it at the end of inventory
    descriptions = []
    for row in oldinv:
        descriptions.append(row[0])

    subcsv_file.seek(0)
    for row2 in consume:
        if row2[0] not in descriptions:
            if row2[0] != "" and row2[0] != "0 Test Point":
                quantity = str((-1)*int(multi)*int(row2[quantity_remove]))
                oldinv.append([row2[0],"","","","","", quantity])

#write inventory in new inventory file
    with open(newcsv, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                            quotechar='"')
        writer.writerows(oldinv)

#close all files
    oldcsv_file.close()
    subcsv_file.close()

########################################################################################################################
# main program

#variable
quantity_old = 6    #position of quantity in inventory
quantity_new = 1    #position of quantity in order
quantity_remove = 2 #position of quantity in BOM

#Enter old inventory csv file name and verify if it is a valid csv file
old = input("Enter old inventory csv file name:")
while True:
    try:
        test = open(old, "r")
        testcsv = csv.reader(test, delimiter=',', quotechar='"')
        test.close()
    except:
        print("This  is not a valid csv file")
        old = input("Enter old inventory csv file name:")
        continue
    break

#Enter new inventory csv name and verify if it is a valid .csv format
newcsv = input("Enter new inventory csv name:")
while True:
    if newcsv[-4:] == ".csv":
        break
    print("This is not a .csv format name")
    newcsv = input("Enter new inventory csv name:")
    continue

#Enter a choice between 1 or 2 and verify it is a valid choice
choice = input("Choose number:\n1)New order items\n2)Remove BOM items\n")
while True:
    if choice == "1" or choice == "2":
        break
    print("This is not a valid choice")
    choice = input("Choose number:\n1)New order items\n2)Remove BOM items\n")
    continue

#if the user choose to enter a new order items csv
if choice == "1":

#Enter order csv file name and verify if it is a valid csv file
    new = input("Enter order CSV file name:")
    while True:
        try:
            test = open(new, "r")
            testcsv = csv.reader(test, delimiter=',', quotechar='"')
            test.close()
        except:
            print("This  is not a valid csv file")
            new = input("Enter order CSV file name:")
            continue
        break

#enter the number of time we add this order to inventory and verify it is a number
    multi = input("How many time:")
    while True:
        try:
            multi = int(multi)
        except:
            print("The number of time is not a number")
            multi = input("How many time:")
            continue
        break

#run add to inventory function
    add(old,newcsv,new,quantity_old,quantity_new, multi)

#if the user choose to remove a BOM items csv
elif choice == "2":

# Enter BOM csv file name and verify if it is a valid csv file
    remove = input("Enter BOM CSV file name:")
    while True:
        try:
            test = open(remove, "r")
            testcsv = csv.reader(test, delimiter=',', quotechar='"')
            test.close()
        except:
            print("This  is not a valid csv file")
            remove = input("Enter BOM CSV file name:")
            continue
        break

#enter the number of time we sub this BOM to inventory and verify it is a number
    multi = input("How many time:")
    while True:
        try:
            multi = int(multi)
        except:
            print("The number of time is not a number")
            multi = input("How many time:")
            continue
        break

#run sub to inventory function
    sub(old,newcsv,remove,quantity_old,quantity_remove,multi)
